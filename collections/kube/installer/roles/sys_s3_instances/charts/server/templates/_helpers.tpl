{{- define "minio-root-password" -}}
{{ printf "{{- with $secret := secret \"%s\" -}}" $.Values.vault.path }}
{{ printf "{{ index $secret.Data.data \"%s\" }}" $.Values.user.password.vault.secretName }}
{{ printf "{{ end }}" }}
{{- end -}}

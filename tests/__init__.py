import glob
import os.path
import sys

from collections.abc import Iterable
from typing import Final


class TestsUtils(object):
    @classmethod
    def format(
        cls,
        tpl: str,
        args: Iterable[object]
    ) -> str:
        return tpl.format(*args)

    @classmethod
    def absolute_dir_path(cls, path: str) -> str:
        abspath = os.path.abspath(path)

        if (abspath == "/"):
            return abspath
        else:
            return TestsUtils.format(
                tpl="{}/",
                args=[
                    abspath
                ]
            )


workspace_relative_path: Final[str] = TestsUtils.format(
    tpl="{}/../../",
    args=[
        __file__
    ]
)

workspace_path: Final[str] = TestsUtils.absolute_dir_path(
    path=workspace_relative_path
)

pattern: Final[str] = TestsUtils.format(
    tpl="{}pkgs/*/lib/",
    args=[
        workspace_path
    ]
)

for pkg_path in glob.glob(pattern):
    sys.path.insert(0, pkg_path)

from __future__ import annotations

import unittest

import kubeinstallerworkspace

from kubeinstallerworkspace.impl import KubeInstallerWorkspaceImpl
from pygenpath.utils import PathUtils
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygenworkspace.impl import WorkspaceDirpathImpl


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    dirpath = PathUtils.dirpath(
        path=TextUtils.format_args(
            tpl="{}/",
            args=[
                __file__
            ]
        )
    )
    workspace_dirpath = dirpath.resolve_dirpath("../../../../")

    test_suite = UnitTestSuiteImpl()

    workspace = KubeInstallerWorkspaceImpl(
        workspace_dirpath=WorkspaceDirpathImpl(
            absolute=workspace_dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./samples/pkgs/{}/",
                    args=[
                        kubeinstallerworkspace.__name__
                    ]
                )
            ),
            relative=PathUtils.dirpath("./")
        )
    )

    workspace.load_tests(test_suite)

    return test_suite


if (__name__ == "__main__"):
    unittest.main()

#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

psh "find ./pkgs/ -mindepth 2 -maxdepth 2 -name setup.py | xargs -I {} dirname {} | xargs -I {} pip install --no-deps {}"

# .post
csh "rm $0"

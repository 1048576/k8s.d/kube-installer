from typing import Final

import ansiblegalaxyworkspacetemplate

from ansiblegalaxyworkspace.abc import AnsibleGalaxyWorkspaceTestCaseLoader
from pygencollection.impl import ArrayImpl
from pygenjinja.abc import JinjaTemplate
from pygenjinja.impl import JinjaTemplateLoaderImpl
from pygenpath.abc import Dirpath
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection
from pygentype.abc import FinalSequence
from pygenworkspace.abc import Workspace


class AnsibleGalaxyWorkspaceImpl(Workspace):
    _entrypoint_template: Final[JinjaTemplate]
    _loaders: FinalCollection[AnsibleGalaxyWorkspaceTestCaseLoader]

    def __init__(
        self,
        workspace_dirpath: Dirpath
    ) -> None:
        template_loader = JinjaTemplateLoaderImpl()

        self._entrypoint_template = template_loader.load(
            module=ansiblegalaxyworkspacetemplate,
            name="entrypoint",
            keep_trailing_newline=False
        )
        self._loaders = ArrayImpl.of()

    def entrypoint(self) -> str:
        return self._entrypoint_template.render()

    def load_tests(self, test_suite: TestSuite) -> None:
        for loader in self._loaders:
            loader.load(test_suite)


__all__: FinalSequence[str] = [
    "AnsibleGalaxyWorkspaceImpl"
]

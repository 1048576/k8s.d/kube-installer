from __future__ import annotations

from pygenerr.err import SystemException
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection


class AnsibleGalaxyWorkspaceTestCaseLoader(object):
    def load(
        self,
        test_suite: TestSuite
    ) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "AnsibleGalaxyWorkspaceTestCaseLoader"
]

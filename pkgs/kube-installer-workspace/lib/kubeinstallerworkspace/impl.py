from __future__ import annotations

from typing import Final

import kubeinstallerworkspacetemplate

from kubeinstallerworkspace.abc import KubeInstallerWorkspaceContext as KIWContext
from kubeinstallerworkspace.abc import KubeInstallerWorkspaceInventoriesDirDataCollector as KIWInventoriesDirDataCollector
from kubeinstallerworkspace.abc import KubeInstallerWorkspaceTestCaseLoader as KIWTestCaseLoader
from kubeinstallerworkspace.datacollector.inventoriesdir.impl import KubeInstallerWorkspaceInventoriesDirDataCollectorImpl as KIWInventoriesDirDataCollectorImpl
from pygencollection.impl import ArrayImpl
from pygenjinja.abc import JinjaTemplate
from pygenjinja.impl import JinjaTemplateLoaderImpl
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection
from pygentype.abc import FinalSequence
from pygenworkspace.abc import Workspace
from pygenworkspace.abc import WorkspaceDirpath


class KubeInstallerWorkspaceImpl(Workspace):
    _entrypoint_template: Final[JinjaTemplate]
    _inventories_dir_data_collector: Final[KIWInventoriesDirDataCollector]
    _loaders: FinalCollection[KIWTestCaseLoader]

    def __init__(
        self,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        template_loader = JinjaTemplateLoaderImpl()

        self._entrypoint_template = template_loader.load(
            module=kubeinstallerworkspacetemplate,
            name="entrypoint",
            keep_trailing_newline=False
        )
        self._inventories_dir_data_collector = \
            KIWInventoriesDirDataCollectorImpl(
                workspace_dirpath=workspace_dirpath
            )
        self._loaders = ArrayImpl.of()

    def entrypoint(self) -> str:
        return self._entrypoint_template.render()

    def load_tests(self, test_suite: TestSuite) -> None:
        context = KIWContext(
            inventories_dir=self._inventories_dir_data_collector.collect()
        )

        for loader in self._loaders:
            loader.load(test_suite, context)


__all__: FinalSequence[str] = [
    "KubeInstallerWorkspaceImpl"
]

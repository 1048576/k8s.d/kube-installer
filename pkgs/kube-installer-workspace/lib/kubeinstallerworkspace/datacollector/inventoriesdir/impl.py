from __future__ import annotations

import os.path

from collections.abc import Iterator
from typing import Final

from kubeinstallerworkspace.abc import KubeInstallerWorkspaceInventoriesDir
from kubeinstallerworkspace.abc import KubeInstallerWorkspaceInventoriesDirDataCollector
from pygencollection.abc import Array
from pygencollection.impl import ArrayImpl
from pygenerr.err import UserException
from pygenos.abc import OSDir
from pygenos.impl import OSDirImpl
from pygenos.utils import OSUtils
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDir
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceFilepath


class KubeInstallerWorkspaceInventoriesDirDataCollectorImpl(
    KubeInstallerWorkspaceInventoriesDirDataCollector
):
    _workspace_dirpath: Final[WorkspaceDirpath]
    _dir: Final[WorkspaceDir]

    def __init__(
        self,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        dirname = "inventories"
        dirpath = workspace_dirpath.resolve_dirpath(
            path=TextUtils.format_args(
                tpl="./{}/",
                args=[
                    dirname
                ]
            )
        )

        self._workspace_dirpath = workspace_dirpath
        self._dir = WorkspaceDir(
            dirname=lambda: dirname,
            dirpath=lambda: dirpath
        )

    def collect(self) -> KubeInstallerWorkspaceInventoriesDir:
        if (os.path.exists(self._dir.dirpath().absolute.path())):
            dirs = ArrayImpl.copy(
                instance=self._find_dirs()
            )

            return KubeInstallerWorkspaceInventoriesDir(
                dir=self._dir,
                dirs=dirs,
                inventory_filepaths=ArrayImpl.copy(
                    instance=self._find_inventory_filepaths(dirs)
                )
            )
        else:
            raise UserException(
                msg=TextUtils.format_args(
                    tpl="Inventories dir doesn't exist [{}]",
                    args=[
                        self._dir.dirpath().absolute.path()
                    ]
                )
            )

    def _find_dirs(self) -> Iterator[OSDir]:
        yield OSDirImpl(
            dirpath=self._workspace_dirpath.absolute,
            dirnames=ArrayImpl.of(self._dir.dirname()),
            filenames=ArrayImpl.of()
        )

        for dirpath in OSUtils.walk(self._dir.dirpath().absolute):
            yield dirpath

    def _find_inventory_filepaths(
        self,
        dirs: Array[OSDir]
    ) -> Iterator[WorkspaceFilepath]:
        for dir in dirs:
            if (dir.dirpath == self._dir.dirpath().absolute):
                root_dir = dir

                break
        else:
            return

        for filename in root_dir.filenames:
            if (filename.endswith(".yaml")):
                yield self._workspace_dirpath.resolve_filepath(
                    path=TextUtils.format_args(
                        tpl="./{}",
                        args=[
                            filename
                        ]
                    )
                )


__all__: FinalCollection[str] = [
    "KubeInstallerWorkspaceInventoriesDirDataCollectorImpl"
]

from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygenos.abc import OSDir
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDataCollector
from pygenworkspace.abc import WorkspaceDir
from pygenworkspace.abc import WorkspaceFilepath


@dataclass
class KubeInstallerWorkspaceInventoriesDir(object):
    dir: Final[WorkspaceDir]
    dirs: FinalCollection[OSDir]
    inventory_filepaths: FinalCollection[WorkspaceFilepath]


class KubeInstallerWorkspaceInventoriesDirDataCollector(
    WorkspaceDataCollector[KubeInstallerWorkspaceInventoriesDir]
):
    ...


@dataclass
class KubeInstallerWorkspaceContext(object):
    inventories_dir: Final[KubeInstallerWorkspaceInventoriesDir]


class KubeInstallerWorkspaceTestCaseLoader(object):
    def load(
        self,
        test_suite: TestSuite,
        context: KubeInstallerWorkspaceContext
    ) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "KubeInstallerWorkspaceContext",
    "KubeInstallerWorkspaceInventoriesDir",
    "KubeInstallerWorkspaceInventoriesDirDataCollector",
    "KubeInstallerWorkspaceTestCaseLoader"
]

#!/usr/bin/env python

import os

from collections.abc import Sequence

import setuptools


def read_requirements() -> Sequence[str]:
    with (open("requirements.txt", "r") as f):
        return f.read().splitlines()


if (__name__ == "__main__"):
    setuptools.setup(
        name="kube-installer-workspace",
        version=os.environ.get("BUILD_ARG_VERSION", "0.0.dev"),
        author="Vladyslav Kazakov",
        author_email="kazakov1048576@gmail.com",
        url="https://gitlab.com/1048576/ansible.d/ansible-collections",
        install_requires=read_requirements(),
        package_data={
            "kubeinstallerworkspace": [
                "py.typed"
            ],
            "kubeinstallerworkspacetemplate": [
                "*.j2",
                "py.typed"
            ]
        },
        package_dir={
            "": "lib"
        },
        packages=setuptools.find_packages("./lib/"),
        entry_points={},
        scripts=[],
        license="MIT"
    )
